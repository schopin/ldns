Source: ldns
Priority: optional
Maintainer: Debian DNS Team <team+dns@tracker.debian.org>
Uploaders: Ondřej Surý <ondrej@debian.org>
Build-Depends: chrpath,
               debhelper-compat (= 13),
               dh-python,
               doxygen,
               libpcap-dev,
               libpython3-dev,
               libssl-dev (>= 1.1.0),
               pkg-config,
               python3-dev:any,
               swig
Standards-Version: 4.3.0.3
Rules-Requires-Root: no
Section: net
Vcs-Browser: https://salsa.debian.org/dns-team/ldns
Vcs-Git: https://salsa.debian.org/dns-team/ldns.git
Homepage: https://www.nlnetlabs.nl/projects/ldns/about/

Package: libldns3
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: ldns library for DNS programming
 The goal of ldns is to simplify DNS programming, it supports recent RFCs
 like the DNSSEC documents, and allows developers to easily create software
 conforming to current RFCs, and experimental software for current Internet
 Drafts.
 .
 This package contains shared libraries.

Package: libldns-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libldns3 (= ${binary:Version}),
         libssl-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Description: ldns library for DNS programming
 The goal of ldns is to simplify DNS programming, it supports recent RFCs
 like the DNSSEC documents, and allows developers to easily create software
 conforming to current RFCs, and experimental software for current Internet
 Drafts.
 .
 This package contains development libraries and headers.

Package: ldnsutils
Section: net
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends}
Recommends: dns-root-data
Description: ldns library for DNS programming
 The goal of ldns is to simplify DNS programming, it supports recent RFCs
 like the DNSSEC documents, and allows developers to easily create software
 conforming to current RFCs, and experimental software for current Internet
 Drafts.
 .
 This package contains various client programs related to DNS that are
 based on top of libldns library and DRILL tool which is similar to dig.
 These tools were designed with DNSSEC in mind and are useful for DNS
 and DNSSEC testing and debugging.

Package: python3-ldns
Section: python
Architecture: any
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Provides: ${python3:Provides}
Multi-Arch: foreign
Description: Python3 bindings for the ldns library for DNS programming
 The goal of ldns is to simplify DNS programming, it supports recent RFCs
 like the DNSSEC documents, and allows developers to easily create software
 conforming to current RFCs, and experimental software for current Internet
 Drafts.
 .
 This archive contains modules that allow you to use LDNS Library in
 Python3 programs.
